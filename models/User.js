import mongoose, { Schema } from 'mongoose';
import timestamps from 'mongoose-timestamp';
import { findOrCreate } from '../db/plugins';

let UserSchema = new Schema({
  id: Number,
  is_bot: Boolean,
  first_name: String,
  last_name: String,
  username: String,
  language_code: String,
  photo: {}
});

UserSchema.methods.toString = function() {
  return `${this.first_name} @${this.username} ${this.last_name}`;
};

// UserSchema.plugin(timestamps);
UserSchema.plugin(findOrCreate);

export default mongoose.model('User', UserSchema);
