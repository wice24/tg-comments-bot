import mongoose, { Schema } from 'mongoose';
import timestamps from 'mongoose-timestamp';
import { findOrCreate } from '../db/plugins';

let MessageSchema = new Schema({
  message_id: Number,
  from: {}, // UserSchema
  date: Number, // Unix,
  chat: {}, // ChatSchema
  media_group_id: String,
  text: String,
  entities: [{}],
  caption_entities: [{}],
  audio: {},
  document: {},
  photo: [{}],
  sticker: {},
  video: {},
  voice: {},
  video_note: {},
  caption: {},
  contact: {},
  location: {}
});

// MessageSchema.plugin(timestamps);
MessageSchema.plugin(findOrCreate);

export default mongoose.model('Message', MessageSchema);
