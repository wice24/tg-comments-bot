import mongoose, { Schema } from 'mongoose';
import timestamps from 'mongoose-timestamp';
import { findOrCreate } from '../db/plugins';
import config from 'config';
import bot from '../bot';

export const POST_TYPES = {
  LOCATION: 'location',
  CONTACT: 'contact',
  VIDEO_NOTE: 'video_note',
  VOICE: 'voice',
  VIDEO: 'video',
  STICKER: 'sticker',
  PHOTO: 'photo',
  DOCUMENT: 'document',
  AUDIO: 'audio',
  ARTICLE: 'article'
};

let PostSchema = new Schema({
  messages: [{
    ref: 'Message',
    type: Schema.Types.ObjectId
  }],
  shares: [{}],
  user: {
    ref: 'User',
    type: Schema.Types.ObjectId
  },
  commentsCount: {
    type: Number,
    default: 0
  },
  ad: {},
  adSecretHash: {
    type: Schema.Types.ObjectId
  }
});

PostSchema.virtual('type').get(function() {
  const { messages: [message] } = this;

  if (message.location) {
    return POST_TYPES.LOCATION;
  }

  if (message.contact) {
    return POST_TYPES.CONTACT;
  }

  if (message.video_note) {
    return POST_TYPES.VIDEO;
  }

  if (message.voice) {
    return POST_TYPES.VOICE;
  }

  if (message.video) {
    return POST_TYPES.VIDEO;
  }

  if (message.sticker) {
    return POST_TYPES.STICKER;
  }

  if (message.photo && message.photo.length > 0) {
    return POST_TYPES.PHOTO;
  }

  if (message.document) {
    return POST_TYPES.DOCUMENT;
  }

  if (message.audio) {
    return POST_TYPES.AUDIO;
  }

  if (message.text) {
    return POST_TYPES.ARTICLE;
  }
});

// Public methods
PostSchema.methods.getType = async function(msg) {
  if (!this.populated('messages')) {
    await this.populate('messages').execPopulate();
  }

  return this.type;
};

PostSchema.methods.sendPost = async function(msg) {
  if (!this.populated('messages')) {
    await this.populate('messages').execPopulate();
  }

  const { messages: [message] } = this;

  const reply_markup = {
    inline_keyboard: [
      ...this._getInlineQueryResultReplyMarkup(true).inline_keyboard,
      ...this.getSwitchInlineReplyMarkup().inline_keyboard
    ]
  };

  await bot.sendMessage(message.chat.id, 'Отлично. Нажмите на кнопку "Publish" и выберите свой канал, чтобы опубликовать пост с комментариями.', {
    reply_markup,
    reply_to_message_id: msg.message_id
  });
};

PostSchema.methods.getInlineQueryResult = function() {
  const { messages: [message] } = this;

  if (message.location) {
    return this._getInlineQueryResult({
      latitude: message.location.latitude,
      longitude: message.location.longitude,
      title: 'Location'
    });
  }

  if (message.contact) {
    return this._getInlineQueryResult({
      phone_number: message.contact.phone_number,
      first_name: message.contact.first_name,
      last_name: message.contact.last_name
    });
  }

  if (message.video_note) {
    return this._getInlineQueryResult({
      video_url: message.video_note.file_url,
      mime_type: 'video/mp4',
      title: 'Video',
      thumb_url: message.video_note.thumb.file_url,
      caption: 'video/mp4'
    });
  }

  if (message.voice) {
    return this._getInlineQueryResult({
      voice_file_id: message.voice.file_id,
      title: 'voice'
    });
  }

  if (message.video) {
    return this._getInlineQueryResult({
      video_file_id: message.video.file_id,
      title: 'Video',
      description: message.video.mime_type,
      caption: message.caption
    });
  }

  if (message.sticker) {
    return this._getInlineQueryResult({
      sticker_file_id: message.sticker.file_id
    });
  }

  if (message.photo.length > 0) {
    return this._getInlineQueryResult({
      photo_file_id: message.photo[0].file_id,
      caption: message.caption
    });
  }

  if (message.document) {
    return this._getInlineQueryResult({
      document_file_id: message.document.file_id,
      title: message.document.file_name,
      description: message.document.mime_type
    });
  }

  if (this.type === POST_TYPES.AUDIO) {
    return this._getInlineQueryResult({
      type: 'audio',
      audio_file_id: message.audio.file_id
    });
  }

  if (this.type === POST_TYPES.ARTICLE) {
    const { entities } = message;

    let { text } = message;

    if (entities) {
      entities.reverse();

      entities.forEach(entity => {
        let markupChar;

        if (entity.type === 'bold') {
          markupChar = '*';
        }

        if (entity.type === 'italic') {
          markupChar = '_';
        }

        if (entity.type === 'code') {
          markupChar = '`';
        }

        text = text.split('');
        text.splice(entity.offset + entity.length, 0, markupChar);
        text.splice(entity.offset, 0, markupChar);
        text = text.join('');
      });
    }

    return this._getInlineQueryResult({
      title: message.text,
      input_message_content: {
        parse_mode: 'Markdown',
        message_text: text
      }
    });
  }
};
PostSchema.methods.getSwitchInlineReplyMarkup = function() {
  return {
    inline_keyboard: [
      [{
        switch_inline_query: this.get('id'),
        text: '✉️ Publish'
      }]
    ]
  };
};

// Inline Query Results
PostSchema.methods._getInlineQueryResultCachedDocument = function() {
  const { messages: [message] } = this;

  return this._getInlineQueryResult({
    type: 'document',
    document_file_id: message.document.file_id
  });
};

PostSchema.methods._getInlineQueryResultCachedAudio = function() {
  const { messages: [message] } = this;

  return this._getInlineQueryResult({
    type: 'audio',
    audio_file_id: message.audio.file_id
  });
};
PostSchema.methods._getInlineQueryResultCachedVoice = function() {
  const { messages: [message] } = this;

  return this._getInlineQueryResult({
    type: 'voice',
    voice_file_id: message.voice.file_id,
    title: 'voice'
  });
};
PostSchema.methods._getInlineQueryResultCachedSticker = function() {
  const { messages: [message] } = this;

  return this._getInlineQueryResult({
    type: 'sticker',
    sticker_file_id: message.sticker.file_id
  });
};
PostSchema.methods._getInlineQueryResultCachedVideo = function() {
  const { messages: [message] } = this;

  return this._getInlineQueryResult({
    type: 'video',
    video_file_id: message.video.file_id,
    title: 'Video',
    description: message.video.mime_type
  });
};
PostSchema.methods._getInlineQueryResultCachedPhoto = function() {
  const { messages: [message] } = this;

  return this._getInlineQueryResult({
    type: 'photo',
    photo_file_id: message.photo[0].file_id,
    caption: message.caption
  });
};
PostSchema.methods._getInlineQueryResultLocation = function() {
  const { messages: [message] } = this;

  return this._getInlineQueryResult({
    type: 'location',
    id: this.get('id'),
    latitude: message.location.latitude,
    longitude: message.location.longitude,
    title: 'Location',
    reply_markup: this._getInlineQueryResultReplyMarkup()
  });
};
PostSchema.methods._getInlineQueryResultContact = function() {
  const { messages: [message] } = this;

  return this._getInlineQueryResult({
    type: 'contact',
    id: this.get('id'),
    phone_number: message.contact.phone_number,
    first_name: message.contact.first_name,
    last_name: message.contact.last_name,
    reply_markup: this._getInlineQueryResultReplyMarkup()
  });
};
PostSchema.methods._getInlineQueryResultArticle = function() {
  const { messages: [message] } = this;
  const { entities } = message;

  let { text } = message;

  if (entities) {
    entities.reverse();

    entities.forEach(entity => {
      if (entity.type === 'bold') {
        text = text.split('');
        text.splice(entity.offset + entity.length, 0, '*');
        text.splice(entity.offset, 0, '*');
        text = text.join('');
      }
    });
  }

  return this._getInlineQueryResult({
    title: message.text,
    input_message_content: {
      parse_mode: 'Markdown',
      message_text: text
    }
  });
};

// Private methods
PostSchema.methods._getInlineQueryResult = function(settings) {
  return {
    ...settings,
    id: this.get('id'),
    type: this.get('type'),
    reply_markup: this._getInlineQueryResultReplyMarkup()
  };
};
PostSchema.methods._getInlineQueryResultReplyMarkup = function(withAdLink) {
  const text = withAdLink ? 'Comments' : `Comments (${this.commentsCount})`;

  return {
    inline_keyboard: [
      [{
        text,
        url: `${config.get('domain')}/posts/${this.get('id')}?v${this.__v}`
      }, withAdLink && {
        text: 'Add advertisement',
        url: `${config.get('domain')}/posts/${this.get('id')}/ad/${this.get('adSecretHash')}`
      }].filter(Boolean)
    ]
  };
};

// Plugins
PostSchema.plugin(timestamps);
PostSchema.plugin(findOrCreate);

export default mongoose.model('Post', PostSchema);
