import User from '../models/User';

export function chain(request, data = {}) {
  const that = this;
  return async function(...args) {
    return new Promise(async (resolve, reject) => {
      async function iter(...args) {
        const fn = args.shift();

        if (!fn) {
          return resolve();
        }

        try {
          await fn.call(that, request, data, async breakChain => {
            if (breakChain) {
              return resolve();
            }
            await iter.apply(that, args);
          });
        } catch (e) {
          reject(e);
        }
      }

      await iter.apply(that, args);
    });
  };
}

export async function privateOnly(msg, data, next) {
  if (msg.chat.type !== 'private') {
    return next(true);
  }

  next();
}

export async function callbackGetUser(query, data, next) {
  const {
    from: {
      id: userId,
      first_name: firstName,
      last_name: lastName,
      username
    }
  } = query;

  const user = await User.findOrCreate({ telegramId: userId }, {
    telegramId: userId,
    firstName,
    lastName,
    username
  });

  data.user = user;
  next();
}

export async function getUser(msg, data, next) {
  const {
    from: {
      id: userId,
      first_name: firstName,
      last_name: lastName,
      username
    }
  } = msg;

  const user = await User.findOrCreate({ telegramId: userId }, {
    telegramId: userId,
    firstName,
    lastName,
    username
  });

  data.user = user;
  next();
}
