import PrettyError from 'pretty-error';
import inlineQuery from './inline-queries/inline-query';
import createPost from './chat-commands/create-post';
import Post from './models/Post';
import bot from './bot';
import './db';

import { updatePostComments } from './webhooks';

// setInterval(updatePostComments, 1000);

const pe = new PrettyError();

bot.on('message', createPost);

bot.on('chosen_inline_result', async query => {
  try {
    const {
      result_id: postId,
      inline_message_id
    } = query;

    await Post.update({
      _id: postId
    }, {
      $push: {
        shares: {
          inline_message_id
        }
      }
    });
  } catch (e) {
    console.log(pe.render(e));
  }
});

bot.on('inline_query', async query => {
  try {
    await inlineQuery(query);
  } catch (e) {
    console.log(pe.render(e));
  }
});
