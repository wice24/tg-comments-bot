import mongoose from 'mongoose';
import Post from '../models/Post';
import bot from '../bot';

import express from 'express';
const app = express();

app.post('/hook/update-count/:postId', async (req, res) => {
  const { postId } = req.params;

  if (!mongoose.Types.ObjectId.isValid(postId)) {
    res.status(400);
    return res.end();
  }

  res.end();

  await Post.update({ _id: postId }, { $inc: { commentsCount: 1 } });

  const post = await Post.findById(postId);

  const { shares } = post;

  shares.forEach(async share => {
    bot.editMessageReplyMarkup(post._getInlineQueryResultReplyMarkup(), {
      inline_message_id: share.inline_message_id
    });
  });
});

app.listen(3001, () => console.log('Example app listening on port 3001!'));

export async function updatePostComments({
  postId = '5a5d285b35bc0b037c30097d'
} = {}) {
  const { shares } = await Post.findById(postId);

  shares.forEach(async share => {
    bot.editMessageReplyMarkup({
      inline_keyboard: [[
        {
          text: `Comments ${Math.random()}`,
          url: `http://google.com?q=${postId}`
        }
      ]]
    }, {
      inline_message_id: share.inline_message_id
    });
  });
}
