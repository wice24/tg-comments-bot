import mongoose from 'mongoose';
import bot from '../bot';
import Post from '../models/Post';
// import Message from '../models/Message';

const { Types: { ObjectId: { isValid } } } = mongoose;

export default async function(query) {
  const {
    id: inline_query_id,
    query: postId
  } = query;

  if (!mongoose.Types.ObjectId.isValid(postId)) {
    return;
  }

  const post = await Post.findById(postId).populate('messages');

  if (isValid(postId)) {
    bot.answerInlineQuery(
      inline_query_id,
      [
        post.getInlineQueryResult()
      ], {
        cache_time: 1
      }
    );
  }
}
