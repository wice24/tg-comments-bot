import mongoose from 'mongoose';
import PrettyError from 'pretty-error';
import Message from '../models/Message';
import Post from '../models/Post';
import User from '../models/User';
import bot from '../bot';
import config from 'config';

const pe = new PrettyError();

export default async function(msg, data) {
  const { chat } = msg;

  if (chat.type !== 'private') {
    return;
  }

  if (msg.text === '/start') {
    return bot.sendMessage(chat.id, 'Пришло время опубликовать первый пост с комментариями.\r\n\r\n1. Отправьте мне текст публикации, фото или видео');
  }

  if (msg.media_group_id) {
    handleAlbumPost(msg);
  } else {
    handlePost(msg);
  }
}

const mediaGroups = {};

async function handleAlbumPost(msg) {
  mediaGroups[msg.media_group_id] = mediaGroups[msg.media_group_id] || [];

  mediaGroups[msg.media_group_id].push(msg);

  setTimeout(async () => {
    try {
      if (mediaGroups[msg.media_group_id]) {
        delete mediaGroups[msg.media_group_id];

        bot.sendMessage(msg.chat.id, 'Sorry, I am not supporting multiple photos for now.');
      }
    } catch (e) {
      console.log(pe.render(e));
    }
  });
}

async function handlePost(msg) {
  try {
    if (msg.voice) {
      return bot.sendMessage(
        msg.chat.id,
        'Я временно не поддерживаю голосовые сообщения.\r\nНачните с текста, фото или видео',
        { reply_to_message_id: msg.message_id }
      );
    }

    if (msg.sticker) {
      return bot.sendMessage(
        msg.chat.id,
        'Я временно не поддерживаю стикеры.\r\nНачните с текста, фото или видео',
        { reply_to_message_id: msg.message_id }
      );
    }

    if (msg.document) {
      return bot.sendMessage(
        msg.chat.id,
        'Я временно не поддерживаю файлы.\r\nНачните с текста, фото или видео',
        { reply_to_message_id: msg.message_id }
      );
    }

    if (msg.audio) {
      return bot.sendMessage(
        msg.chat.id,
        'Я временно не поддерживаю аудио файлы.\r\nНачните с текста, фото или видео',
        { reply_to_message_id: msg.message_id }
      );
    }

    if (msg.video_note) {
      return bot.sendMessage(
        msg.chat.id,
        'Я временно не поддерживаю видеозаписи.\r\nНачните с текста, фото или видео',
        { reply_to_message_id: msg.message_id }
      );
    }

    if (msg.location) {
      return bot.sendMessage(
        msg.chat.id,
        'Я временно не поддерживаю локации.\r\nНачните с текста, фото или видео',
        { reply_to_message_id: msg.message_id }
      );
    }

    if (msg.contact) {
      return bot.sendMessage(
        msg.chat.id,
        'Я временно не поддерживаю контакты.\r\nНачните с текста, фото или видео',
        { reply_to_message_id: msg.message_id }
      );
    }

    let user = await User.findOrCreate({ id: msg.from.id }, msg.from);

    if (msg.photo) {
      const files = await Promise.all(msg.photo.map(p => {
        return bot.getFile(p.file_id);
      }));

      msg.photo = msg.photo.map((photo, index) => ({
        ...photo,
        ...files[index],
        file_url: `https://api.telegram.org/file/bot${config.get('botToken')}/${files[index].file_path}`
      }));
    }

    if (msg.video) {
      let video = await bot.getFile(msg.video.file_id);
      let thumb = await bot.getFile(msg.video.thumb.file_id);

      msg.video.file_path = video.file_path;
      msg.video.file_url = `https://api.telegram.org/file/bot${config.get('botToken')}/${video.file_path}`;

      msg.video.thumb.file_path = video.file_path;
      msg.video.thumb.file_url = `https://api.telegram.org/file/bot${config.get('botToken')}/${thumb.file_path}`;
    }

    const { insertedIds } = await Message.collection.insert([msg]);

    const post = await Post.create({
      messages: insertedIds,
      user: user._id,
      adSecretHash: mongoose.Types.ObjectId()
    });

    await post.sendPost(msg);
  } catch (e) {
    console.log(e);
    console.log(pe.render(e));
    pe.render(e);

    bot.sendMessage(
      msg.chat.id,
      `Something went wrong, please try again or contact as at ${config.get('email')}`
    );
  }
}
