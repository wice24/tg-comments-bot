import constantMirror from 'constant-mirror';

export default {
  CREATE_POST_PATTERN: new RegExp(/.*/),
  TEST: constantMirror('TEST', 'TEST1')
};
